package com.itau.jogoVelha.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.jogoVelha.exceptions.JogoVelhaException;
import com.itau.jogoVelha.models.Jogada;
import com.itau.jogoVelha.models.Jogador;
import com.itau.jogoVelha.models.JogoVelha;
import com.itau.jogoVelha.models.Mensagem;

@RestController
public class JogoVelhaController {

	Jogador jogador1 = new Jogador(0, "X");
	Jogador jogador2 = new Jogador(1, "O");
	JogoVelha jogoVelha = new JogoVelha(jogador1);

	@RequestMapping(method = RequestMethod.GET, path = "/jogo")
	public ResponseEntity<JogoVelha> getJogoVelha() {
		return ResponseEntity.ok(jogoVelha);
	}

	@RequestMapping(method = RequestMethod.POST, path = "/jogar")
	public ResponseEntity<?> getJogoVelha(@RequestBody Jogada jogada) {
		try {
			jogoVelha.jogar(jogada.getX(), jogada.getY());
			if (jogoVelha.getJogador() == jogador1) {
				jogoVelha.setJogador(jogador2);
			} else {
				jogoVelha.setJogador(jogador1);
			}
			return ResponseEntity.ok(jogoVelha);
		} catch (JogoVelhaException e) {
			Mensagem msg = new Mensagem(e.getMessage());
			return ResponseEntity.badRequest().body(msg);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/iniciar")
	public ResponseEntity<?> iniciar() {
		try {
			jogoVelha.reiniciar(jogador1);
			return ResponseEntity.ok(jogoVelha);
		} catch (JogoVelhaException e) {
			Mensagem msg = new Mensagem(e.getMessage());
			return ResponseEntity.badRequest().body(msg);
		}
	}

}
