package com.itau.jogoVelha.exceptions;

public class JogoVelhaException extends Exception {	
	public JogoVelhaException(String mensagem) {
		super(mensagem);
	}
}
