package com.itau.jogoVelha.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.itau.jogoVelha.exceptions.JogoVelhaException;

public class JogoVelha {
	@JsonIgnore
	private Jogador jogador;
	
	private int[] placar;
	private String[][] tabuleiro;
	private boolean encerrado;
	
	@JsonIgnore
	private int rodada;
		
	public JogoVelha(Jogador jogador) {
		this.jogador = jogador;
		this.placar = new int[2];
		this.tabuleiro = new String[3][3];
		limparTabuleiro();
		this.rodada = 1;
		this.encerrado = false;
	}
	
	public void jogar(int x, int y) throws JogoVelhaException {
		if (this.encerrado) {
			throw new JogoVelhaException("Essa rodada já foi encerrada, inicie uma nova para continuar.");
		}
		if (!this.tabuleiro[x][y].equals("")) {
			throw new JogoVelhaException("Essa casa não está disponível para jogar. Tente novamente.");
		}				
		this.tabuleiro[x][y] = this.jogador.getIdentificador();
		verificarGanhador();
	}
	
	private void verificarGanhador() {
		if (calcularHorizontal("X") || calcularVertical("X") || calcularDiagonal("X")) {
			adicionarVitoria();
			this.encerrado = true;
		}
		if (calcularHorizontal("O") || calcularVertical("O") || calcularDiagonal("O")) {
			adicionarVitoria();
			this.encerrado = true;
		}		
		if (getQuantidadeCasasDiponiveis() == 0 && !this.encerrado) {
			this.encerrado = true;
		}
	}
	
	private void adicionarVitoria() {
		this.jogador.adicionarVitoria();
		if (jogador.getIndice() == 0) {
			this.placar[0]++;
		} else {
			this.placar[1]++;
		}
	}
	
	private int getQuantidadeCasasDiponiveis() {
		int cont = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (this.tabuleiro[i][j].equals("")) {
					cont++;
				}
			}
		}
		return cont;
	}
	
	private boolean calcularHorizontal(String identificador) {
		int cont = 0; 
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (this.tabuleiro[i][j].equals(identificador)) {
					cont++;
				}
			}
			if (cont == 3) {
				return true;
			}
			cont = 0;
		}
		return false;
	}
	
	private boolean calcularVertical(String identificador) {
		int cont = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (this.tabuleiro[j][i].equals(identificador)) {
					cont++;
				}
			}
			if (cont == 3) {
				return true;				
			}
			cont = 0;
		}
		return false;
	}
	
	private boolean calcularDiagonal(String identificador) {
		int cont = 0;
		if (this.tabuleiro[0][0].equals(identificador)) cont++;
		if (this.tabuleiro[1][1].equals(identificador)) cont++;
		if (this.tabuleiro[2][2].equals(identificador)) cont++;
		
		if (cont == 3) return true;
		cont = 0;
		
		if (this.tabuleiro[0][2].equals(identificador)) cont++;
		if (this.tabuleiro[1][1].equals(identificador)) cont++;
		if (this.tabuleiro[0][0].equals(identificador)) cont++;
		
		if (cont == 3) return true;
		
		return false;
	}
	
	public void reiniciar(Jogador jogadorInicial) throws JogoVelhaException {
		if (this.rodada == 1 && !this.encerrado)
			verificarGanhador();
		
		if (!this.encerrado) {
			throw new JogoVelhaException("Não é possível iniciar uma nova rodada pois a atual ainda está em andamento.");
		}		
		
		// Limpar o tabuleiro
		limparTabuleiro();
		
		this.jogador = jogadorInicial;
		
		this.encerrado = false;
				
		// Reiniciar a rodada
		this.rodada = 1;
	}

	public int[] getPlacar() {
		return placar;
	}

	public String[][] getTabuleiro() {
		return tabuleiro;
	}			
	
	public Jogador getJogador() {
		return this.jogador;
	}
	
	public void setJogador(Jogador jogador) {
		this.jogador = jogador;
	}
	
	public int getRodada() {
		return this.rodada;
	}
	
	public boolean getEncerrado() {
		return this.encerrado;
	}
	
	private void limparTabuleiro() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				this.tabuleiro[i][j] = "";
			}
		}
	}
	
	public void finalizarRodada() {
		this.rodada++;
	}
}
