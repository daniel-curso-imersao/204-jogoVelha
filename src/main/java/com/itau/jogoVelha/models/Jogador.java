package com.itau.jogoVelha.models;

public class Jogador {
	private int indice;
	private int pontuacao;	
	private String identificador;
	
	public Jogador(int indice, String identificador) {
		this.indice = indice;
		this.identificador = identificador;
	}
	
	public int getPontuacao() {
		return pontuacao;
	}
	public String getIdentificador() {
		return identificador;
	}
	
	public void adicionarVitoria() {
		this.pontuacao++;
	}

	public int getIndice() {
		return indice;
	}		
}
